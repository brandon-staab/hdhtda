\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{commath}
\usepackage{float}
\usepackage[margin=1in]{geometry}
\usepackage{mathtools}
\usepackage{pgfplots}
\usepackage{titlesec}
\pgfplotsset{compat=1.5}

\author{Brandon Staab \\ Professor Datta}
\title{Principal Component Analysis}

\newcommand{\matrixA}{
\begin{bmatrix*}[r]
13 & -4 &  2 \\ 
-4 & 13 & -2 \\ 
2 & -2 & 10 \\
\end{bmatrix*}
}


\newcommand{\matrixI}{
\begin{bmatrix*}[r]
1 & & \\ 
& 1 & \\ 
& & 1 \\
\end{bmatrix*}
}

\newcommand{\matrixD}{
\begin{bmatrix}
18 & & \\
 & 9 & \\
 & & 9 \\
\end{bmatrix}
}

\newcommand{\matrixP}{
\begin{bmatrix*}[r]
 \dfrac{2}{3} &                  0  & -\dfrac{\sqrt{5}}{3}  \\[12pt]
-\dfrac{2}{3} & \dfrac{1}{\sqrt{5}} & -\dfrac{4}{3\sqrt{5}} \\[12pt]
 \dfrac{1}{3} & \dfrac{2}{\sqrt{5}} &  \dfrac{2}{3\sqrt{5}}
\end{bmatrix*}
}

\newcommand{\matrixPT}{
\begin{bmatrix*}[r]
\dfrac{2}{3}         & -\dfrac{2}{3}         & \dfrac{1}{3}         \\[12pt]
0                    &  \dfrac{1}{\sqrt{5}}  & \dfrac{2}{\sqrt{5}}  \\[12pt]
-\dfrac{\sqrt{5}}{3} & -\dfrac{4}{3\sqrt{5}} & \dfrac{2}{3\sqrt{5}} \\[12pt]
\end{bmatrix*}
}

\newcommand{\vecE}[1]{
\begin{pmatrix}
e_{0,#1} \\ 
e_{1,#1} \\ 
e_{2,#1}
\end{pmatrix}
}

\newcommand{\vecV}[1]{
\begin{pmatrix}
x_{#1} \\ 
y_{#1} \\ 
z_{#1}
\end{pmatrix}
}

\begin{document}
\maketitle
\tableofcontents
\newpage

\section{Spectral Decomposition}
\subsection{Instructions}
\paragraph{Given A = $ \matrixA{} $, find $P$ and $D$ such that:}

\begin{enumerate}
\item
$ \textbf{A} = \textbf{PDP}^{\mathsf{T}} $

\item
$ \lambda_{i} $ is an eigenvalue of \textbf{A}

\item
$ \begin{vmatrix}\textbf{A} - \lambda_{i}\textbf{I}\end{vmatrix} = 0$

\item
$ \vec{\lambda} = 
\begin{pmatrix}
\lambda_{0} \\ 
\lambda_{1} \\ 
\lambda_{2}
\end{pmatrix} $

\item
$ D =
\vec{\lambda}\textbf{I} =
\begin{bmatrix}
\lambda_{0}          &           & \\ 
         & \lambda_{1}           & \\ 
         &           & \lambda_{2} \\
\end{bmatrix} $

\item
$ \lambda_{i} \geq \lambda_{i+1}$

\item
$ \vec{e}_{i} $ is an orthonormal eigenvector of \textbf{A}

\item $ \textbf{P} =
\begin{bmatrix} \vec{e}_{0} & \vec{e}_{1} & \vec{e}_{2} \end{bmatrix} = 
\begin{bmatrix}
\vecE{0}
\vecE{1}
\vecE{2}
\end{bmatrix} $
\end{enumerate}

\subsection{Compute the Diagonal Matrix of Eigenvalues}

\subsubsection{Review of Determinants}

\paragraph{$ 2 \times 2 $ Matrix Formula}
\begin{equation}\begin{split}
\begin{vmatrix}
a & b \\ 
c & d
\end{vmatrix} =
ad - bc
\end{split}\end{equation}

\paragraph{$ 3 \times 3 $ Matrix Formula}
\begin{equation}\begin{split}
\begin{vmatrix}
a & b & c \\ 
d & e & f \\ 
g & h & i
\end{vmatrix}
= a
\begin{vmatrix}
\square & \square & \square \\ 
\square & e & f \\ 
\square & h & i
\end{vmatrix}
- b
\begin{vmatrix}
a & \square & c \\ 
\square & \square & \square \\ 
g & \square & i
\end{vmatrix}
+ c
\begin{vmatrix}
a & b & \square \\ 
d & e & \square \\ 
\square & \square & \square
\end{vmatrix}
\end{split}\end{equation}

\begin{equation}\begin{split}
\begin{vmatrix}
a & b & c \\ 
d & e & f \\ 
g & h & i
\end{vmatrix}
= a
\begin{vmatrix}
e & f \\ 
h & i
\end{vmatrix}
- b
\begin{vmatrix}
a & c \\ 
g & i
\end{vmatrix}
+ c
\begin{vmatrix}
a & b \\ 
d & e \\
\end{vmatrix}
\end{split}\end{equation}
\begin{equation}\begin{split}
\begin{vmatrix}
a & b & c \\ 
d & e & f \\ 
g & h & i
\end{vmatrix}
= aei + bfg + cdh - ceg - bdi - afh
\end{split}\end{equation}

\subsubsection{Find the Characteristic Polynomial}

\paragraph{Find when $ \begin{vmatrix}\textbf{A} - \lambda\textbf{I}\end{vmatrix} = 0 $}
\begin{equation}\begin{split}
\begin{vmatrix} \matrixA{} - \lambda \matrixI{} \end{vmatrix} = 0
\end{split}\end{equation}

\begin{equation}\begin{split}
\begin{vmatrix} 
\begin{bmatrix*}[r]
13 - \lambda & -4          &  2 \\ 
-4           & 13 -\lambda & -2 \\ 
 2           & -2          & 10 - \lambda \\
\end{bmatrix*}
\end{vmatrix} = 0
\end{split}\end{equation}


\begin{equation}\begin{split}
  [(13-\lambda)(13-)(10)]
+ [(-4)(-2)(2)]
+ [(2)(-4)(-2)]  \\
- [(2)(13-\lambda)(2)]
- [(-2)(-2)(13-\lambda)]
- [(10-\lambda)(-4)(-4)]
= 0
\end{split}\end{equation}

\paragraph{Simplify the Equation}
\begin{equation}\begin{split}
  [(169 - 26\lambda + \lambda^{2})(10 - \lambda)]
+ [16]
+ [16]
- [52 -4\lambda]
- [52 - 4\lambda]
- [160 - 16\lambda]
= 0
\end{split}\end{equation}

\paragraph{Characteristic Polynomial:}
\begin{equation}\begin{split}
-\lambda^{3} + 36\lambda^{2} - 405\lambda + 1458 = 0
\end{split}\end{equation}

\subsubsection{Find the Roots of the Characteristic Polynomial}

\paragraph{List All Possible Roots Using the Rational Root Theorem}
\begin{equation}\begin{split}
a = -1, d = 1458
\end{split}\end{equation}

\begin{tabular}{|c||c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline 
Factors(a) & -1 & \multicolumn{13}{c|}{} \\ 
\hline 
Factors(d) & 1 & 2 & 3 & 6 & 9 & 18 & 27 & 54 & 81 & 162 & 243 & 486 & 729 & 1458 \\ 
\hline 
\end{tabular} 

\paragraph{Possible Roots:}
\begin{equation}\begin{split}
\begin{array}{ccccccc}
\pm1  & \pm2  & \pm3   & \pm6   & \pm9   & \pm18  & \pm27   \\
\pm54 & \pm81 & \pm162 & \pm243 & \pm486 & \pm729 & \pm1458
\end{array}
\end{split}\end{equation}

\paragraph{Find the Roots by dividing the characteristic polynomial by a possible root using synthetic division:}
\begin{equation}\begin{split}
\renewcommand\arraystretch{1.5}
\setlength\doublerulesep{0pt}
\begin{array}{rrrrr}
\multicolumn{1}{r|}
9 & -1 & 36 & -405 &  1458 \\ \cline{2-5}
  &    & -9 &  243 & -1458 \\ \cline{2-5}
  & -1 & 27 & -162 &     0
\end{array}
\end{split}\end{equation}

\begin{equation}\begin{split}
\lambda = 9
\end{split}\end{equation}

\paragraph{Write the remaining quotient and then solve by factoring:}
\begin{equation}\begin{split}
-\lambda^{2} + 27\lambda - 162\lambda = 0
\end{split}\end{equation}

\begin{equation}\begin{split}
\lambda^{2} - 27\lambda + 162\lambda = 0
\end{split}\end{equation}

\begin{equation}\begin{split}
(\lambda - 9)(\lambda - 18) = 0
\end{split}\end{equation}

\subsubsection{Eigenvalues}
\begin{equation}\begin{split}
\vec{\lambda} = \begin{pmatrix}
18 \\
9 \\
9
\end{pmatrix} 
\end{split}\end{equation}

\subsubsection{Diagonal Matrix D of Eigenvalues}

\begin{equation}\begin{split}
\textbf{D} = \vec{\lambda}\textbf{I} = \matrixD
\end{split}\end{equation}

\subsection{Find Nontrivial Eigenvectors}
\subsubsection{Simplify Equation of Eigenvector for Gaussian Elimination}

\begin{equation}\begin{split}
\textbf{A}\vec{v}_{i} = \lambda_{i}\vec{v}_{i}
\end{split}\end{equation}

\begin{equation}\begin{split}
\matrixA\vecV{i} = \lambda_{i}\vecV{i}
\end{split}\end{equation}

\begin{equation}\begin{split}
\begin{bmatrix*}[r]
13x_{i} & -4y_{i} &  2z_{i} \\
-4x_{i} & 13y_{i} & -2z_{i} \\
 2x_{i} & -2y_{i} & 10z_{i}
\end{bmatrix*} =
\begin{pmatrix}
\lambda_{i}x_{i} \\
\lambda_{i}y_{i} \\
\lambda_{i}z_{i}
\end{pmatrix} 
\end{split}
\end{equation}

\begin{equation}\begin{split}
\begin{bmatrix*}[r]
13x_{i} - \lambda_{i} & -4y_{i}               &  2z_{i}              \\
              -4x_{i} & 13y_{i} - \lambda_{i} & -2z_{i}              \\
               2x_{i} & -2y_{i}               & 10z_{i} - \lambda_{i}
\end{bmatrix*}
= 0
\end{split}\end{equation}

\begin{equation}\begin{split}
\left[\begin{array}{rrr|r}  
13 - \lambda_{i} & -4               &  2               & 0 \\ 
              -4 & 13 - \lambda_{i} & -2               & 0 \\ 
               2 & -2               & 10 - \lambda_{i} & 0
\end{array}\right]
\end{split}\end{equation}

\subsubsection{Solve for Eigenvectors}

\paragraph{Find eigenvector corresponding to $ \lambda_{0} = 18 $ }
\begin{equation}\begin{split}
\left[\begin{array}{rrr|r}  
-5 & -4 &  2 & 0 \\
-4 & -5 & -2 & 0 \\
 2 & -2 & -8 & 0 \\
\end{array}\right]
\end{split}\end{equation}

\begin{equation}\begin{split}
\left[\begin{array}{rrr|r}  
1 & 0 & -2 & 0 \\
0 & 1 &  2 & 0 \\
0 & 0 &  0 & 0 \\
\end{array}\right]
\end{split}\end{equation}

\begin{equation}\begin{split}\begin{cases}
x - 2z = 0 \\
y + 2z = 0
\end{cases}\end{split}\end{equation}

\begin{equation}\begin{split}\begin{cases}
x =  2z \\
y = -2z
\end{cases}\end{split}\end{equation}


\begin{equation}\begin{split}
\vec{v}_0 =
\begin{pmatrix*}[r]
 2z \\ 
-2z \\ 
  z
\end{pmatrix*}
\end{split}\end{equation}

\paragraph{Let $ z = 1 $}
\begin{equation}\begin{split}
\vec{v}_0^{\,\star} =
\begin{pmatrix*}[r]
 2 \\ 
-2 \\ 
 1
\end{pmatrix*}
\end{split}\end{equation}

\paragraph{Find eigenvectors corresponding to $ \lambda_{1}, \lambda_{2} = 9 $ }
\begin{equation}\begin{split}
\left[\begin{array}{rrr|r}  
 4 & -4 &  2 & 0 \\
-4 &  4 & -2 & 0 \\
 2 & -2 &  1 & 0 \\
\end{array}\right]
\end{split}\end{equation}

\begin{equation}\begin{split}
\left[\begin{array}{rrr|r}  
1 & 1 & \dfrac{1}{2} & 0 \\
0 & 0 &            0 & 0 \\
0 & 0 &            0 & 0 \\
\end{array}\right]
\end{split}\end{equation}

\begin{equation}\begin{split}
x - y + \dfrac{z}{2} = 0
\end{split}\end{equation}

\begin{equation}\begin{split}
x = y - \dfrac{z}{2}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{v}_{1}, \vec{v}_{2} =
\begin{pmatrix*}[l]
y - \dfrac{z}{2} \\ 
y \\ 
z
\end{pmatrix*}
\end{split}\end{equation}

\paragraph{Let $ y = 1 $ and $ z = \lbrace2, 4\rbrace $}
\begin{equation}\begin{split}
\vec{v}_{1}^{\,\star} =
\begin{pmatrix}
0 \\ 
1 \\ 
2
\end{pmatrix}
\qquad
\vec{v}_{2}^{\,\star} =
\begin{pmatrix*}[r]
-1 \\ 
 1 \\ 
 4
\end{pmatrix*}
\end{split}\end{equation}

\subsection{Find Orthonormal Eigenvectors}

\subsubsection{The Gram-Schmidt Algorithm for Orthonormalization}

\paragraph{Base Case:}
\begin{equation}\begin{split}
\vec{e}_{0} = \dfrac{\vec{v}_{0}^{\, \star}}{\norm{\vec{v}_{0}^{\, \star}}}
\end{split}\end{equation}

\paragraph{Iteration:}
\begin{equation}\begin{split}
\vec{u}_{i} = \vec{v}_{i}^{\, \star} - \left(\vec{v}_{i}^{\, \star}\cdot\vec{e}_{i-1}\right)\vec{e}_{i-1}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{e}_{i} = \dfrac{\vec{u}_{i}}{\norm{\vec{u}_{i}}}
\end{split}\end{equation}

\subsubsection{Apply Gram-Schmidt on the Eigenvectors $ \vec{v}_{i}^{\, \star} $ to Obtain $ \vec{e}_{i} $ }

\paragraph{Base Case:}
\begin{equation}\begin{split}
\vec{e}_{0} = 
\begin{pmatrix*}[r]
 \dfrac{2}{3} \\[12pt]
-\dfrac{2}{3} \\[12pt]
 \dfrac{1}{3}
\end{pmatrix*}
\end{split}\end{equation}

\paragraph{First Iteration:}
\begin{equation}\begin{split}
\vec{u}_{1} = \vec{v}_{1}^{\, \star} - \left(\vec{v}_{1}^{\, \star}\cdot\vec{e}_{0}\right)\vec{e}_{0}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{1} = \vec{v}_{1}^{\, \star} - \left(
0\times\dfrac{2}{3} + 1\times-\dfrac{2}{3} + 2\times\dfrac{1}{3}
\right)\vec{e}_{1}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{1} = \vec{v}_{1}^{\, \star} - \left(
-\dfrac{2}{3} + \dfrac{2}{3}
\right)\vec{e}_{1}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{1} = \vec{v}_{1}^{\, \star} =
\begin{pmatrix*}[r]
0 \\ 
1 \\ 
2
\end{pmatrix*}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{e}_{1} =
\dfrac{\vec{u}_{1}}{\norm{\vec{u}_{1}}} =
\begin{pmatrix}
0 \\[4pt]
\dfrac{1}{\sqrt{5}} \\[12pt]
\dfrac{2}{\sqrt{5}}
\end{pmatrix}
\end{split}\end{equation}

\paragraph{Second Iteration:}
\begin{equation}\begin{split}
\vec{u}_{2} = \vec{v}_{2}^{\, \star} - \left(\vec{v}_{2}^{\, \star}\cdot\vec{e}_{1}\right)\vec{e}_{1}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{2} = \vec{v}_{2}^{\, \star} - \left(
-1\times0 + 1\times\dfrac{1}{\sqrt{5}} + 4\times\dfrac{2}{\sqrt{5}}
\right)\vec{e}_{1}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{2} = \vec{v}_{2}^{\, \star} - \dfrac{9}{\sqrt{5}}
\begin{pmatrix}
0 \\[4pt]
\dfrac{1}{\sqrt{5}} \\[12pt]
\dfrac{2}{\sqrt{5}}
\end{pmatrix}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{2} = 
\begin{pmatrix*}[r]
-1 \\ 
 1 \\ 
 4
\end{pmatrix*}
-
\begin{pmatrix}
0 \\[4pt]
\dfrac{9}{5} \\[12pt]
\dfrac{18}{5}
\end{pmatrix}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{u}_{2} = 
\begin{pmatrix*}[r]
-1 \\[4pt]
-\dfrac{4}{5} \\[12pt]
\dfrac{2}{5}
\end{pmatrix*}
\end{split}\end{equation}

\begin{equation}\begin{split}
{\norm{\vec{u}_{2}}} =
\sqrt{\left(-1\right)^{2} + \left(-\dfrac{4}{5}\right)^{2} + \left(\frac{2}{5}\right)^{2}} =
\sqrt{\dfrac{25}{25} + \dfrac{16}{25} + \dfrac{4}{25}} = 
\sqrt{\dfrac{45}{25}} =
\sqrt{\dfrac{9}{5}} =
\dfrac{3}{\sqrt{5}}
\end{split}\end{equation}

\begin{equation}\begin{split}
\vec{e}_{2} = 
\dfrac{\vec{u}_{2}}{\norm{\vec{u}_{2}}} =
\begin{pmatrix*}[r]
-\dfrac{\sqrt{5}}{3} \\[12pt]
-\dfrac{4}{3\sqrt{5}} \\[12pt]
 \dfrac{2}{3\sqrt{5}}
\end{pmatrix*} 
\end{split}\end{equation}

\newpage
\subsubsection{Make Orthogonal Matrix From Processed Eigenvectors}

\begin{equation}\begin{split}
\textbf{P} =
\begin{bmatrix} \vec{e}_{0} & \vec{e}_{1} & \vec{e}_{2} \end{bmatrix} = 
\begin{bmatrix}
\begin{pmatrix*}[r]
 \dfrac{2}{3} \\[12pt]
-\dfrac{2}{3} \\[12pt]
 \dfrac{1}{3}
\end{pmatrix*}
\begin{pmatrix}
0 \\[4pt]
\dfrac{1}{\sqrt{5}} \\[12pt]
\dfrac{2}{\sqrt{5}}
\end{pmatrix}
\begin{pmatrix*}[r]
-\dfrac{\sqrt{5}}{3} \\[12pt]
-\dfrac{4}{3\sqrt{5}} \\[12pt]
 \dfrac{2}{3\sqrt{5}}
\end{pmatrix*} 
\end{bmatrix} =
\matrixP{}
\end{split}\end{equation}

\subsection{Check that $ \textbf{A} = \textbf{PDP}^{\mathsf{T}} $}

\begin{equation}\begin{split}
\matrixA = \matrixP\matrixD\matrixPT
\end{split}\end{equation}

\section{Principal Component Analysis}
\subsection{Locate Elbow on Scree Plot}

\begin{figure}[H]
\begin{tikzpicture}
\begin{axis}[
	xlabel=Component Index,
	ylabel=Eigenvalue,
	xmin=-0.2,
	xmax=2.2,
	ymin=-2,
	ymax=20,
	xtick={0,1,2,3},
	ytick={0,9,18},
	width=1.0\textwidth,
	height=0.7\textwidth,
	domain=0:2
]
\addplot[color=blue, dotted] {9};
\addplot[mark=*] coordinates {(0,18) (1,9) (2,9)};
\node[pin=45:{Elbow}] at (axis cs:1,9) {};
\end{axis}
\end{tikzpicture}
\end{figure}

\end{document}
